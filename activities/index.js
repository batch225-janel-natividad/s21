/*
let arrayName = ['Dwayne Johnson','Steve Austin', 'Kurt Angle', 'Dave Bautista', ];
console.log("Original Array:")
console.log(arrayName);
arrayName.push('John Cena');
console.log(arrayName);
console.log(arrayName[2]);
console.log(arrayName[4]);
arrayName.length--;
console.log(arrayName);
arrayName.push('Triple H');
console.log(arrayName)

let arrayZero =[];
console.log(arrayZero);

function isArrayEmpty() {
    return !arrayZero.length;
}

let isUsersEmpty = isArrayEmpty();
console.log(isUsersEmpty);


*/


let users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];

console.log("Original Array:")
console.log(users);


function singleArg () {
	users.push('John Cena');
	console.log(users);
}
singleArg();


function index () {
	console.log(users[2]);
	console.log(users[4]);
}

index();

function deleteItem() {
	users.length = users.length-1;
	console.log(users);
}
deleteItem();

function addItem() {
	users.push('Triple H');
	console.log(users);
}
addItem();

function deleteAll(){
	users.length=0;
	console.log(users);
}
deleteAll();

function isArrayEmpty() {
    return !users.length;
}

let isUsersEmpty = isArrayEmpty();
console.log(isUsersEmpty);
